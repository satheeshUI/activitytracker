import React from 'react';
import { Panel, Avatar } from "rsuite";
import CustomModal from './Modal';

class User extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        showCustomModal: false,
        activeUser: {}
      }
    }

    // close custom modal
    closeCustomModal = (evt) => {
      this.setState({ showCustomModal: false, activeUser: {} })
    }

    // open custom modal
    openCustomModal = (person) => {
      this.setState({ showCustomModal: true, activeUser: person })
    }

    render() {
      const { UserList } = this.props
      const { showCustomModal, activeUser } = this.state
      return (
        <React.Fragment>
        {
          UserList.map((person, userIndex) => {
            return <PanelComponent
                      key={userIndex}
                      person={person}
                      showCustomModal={showCustomModal}
                      openCustomModal={this.openCustomModal}
                      closeCustomModal={this.closeCustomModal}
                      activeUser={activeUser} />
          })
        }
       </React.Fragment>
      );
    }
  }

  function PanelComponent(props) {
    const { person, showCustomModal, openCustomModal, closeCustomModal, activeUser } = props
    return(
      <Panel bordered className="usercard_panel">
        <div className="usercard_panel__inner" onClick={() => openCustomModal(person)}>
          <div className="usercard_panel__inner__avatar">
            <Avatar circle>{person.real_name.substring(0, 1)}</Avatar>
          </div>
          <div className="usercard_panel__inner__details">
            <span className="d-block usercard_panel__inner__details__title">{ person.real_name }</span>
            <span className="d-block">{ person.tz }</span>
          </div>
        </div>
        <CustomModal
          showCustomModal={showCustomModal}
          closeCustomModal={closeCustomModal}
          activeUser={activeUser} />
      </Panel>
    )
  }
  
export default User