import React from 'react';
import { Whisper, Popover, Badge, Calendar } from 'rsuite';
import _ from 'lodash';

var realData = {}

function getTodoList(date) {
  let data = _.filter(realData.activity_periods, el => {
    return el.start_time.substring(0, 11) === date.toString().substring(4, 15)
  })  
  return data.length ? data : []
}
  
  function renderCell(date) {
      const list = getTodoList(date);
      const displayList = list.filter((item, index) => index < 2);
      if (list.length) {
      const moreCount = list.length;
      const moreItem = (
        <li>
          <Whisper
            placement="top"
            trigger="click"
            speaker={
              <Popover>
                <h6 className="activity-title">Active Time:</h6>
                {list.map((item, index) => (
                  <div key={index}>
                    <p>
                      <b>{item.start_time.split(" ")[3]} - {item.end_time.split(" ")[3]}</b>
                    </p>
                  </div>
                ))}
              </Popover>
            }
          >
            <a>{moreCount} more</a>
          </Whisper>
        </li>
      );
  
      return (
        <ul className="calendar-todo-list">
          {displayList.map((item, index) => (
              <li key={index}>
              <Badge /> { item.start_time.split(" ")[3] } - { item.end_time.split(" ")[3] }
            </li>
          ))}
          {moreCount ? moreItem : null}
        </ul>
      );
    }  
    return null;
  }
  
function CustomCalendar(props) {
    realData = props.activeUser
    return (
        <Calendar bordered renderCell={renderCell} />
    )
}


export default CustomCalendar