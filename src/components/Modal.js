import React from 'react';
import { Button, Modal  } from 'rsuite';
import CustomCalendar from "./CustomCalendar";

class CustomModal extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        overflow: true
      };
    }
    render() {
      const { overflow } = this.state;
      const { showCustomModal, closeCustomModal, activeUser } = this.props;
      return (
        <div className="modal-container">
          <Modal overflow={overflow} show={showCustomModal} onHide={() => closeCustomModal()}>
            <Modal.Header>
                <Modal.Title>{ activeUser.real_name }
            </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="timetracker-header">
                <CustomCalendar activeUser={activeUser} />
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => closeCustomModal()} appearance="primary">Ok</Button>
              <Button onClick={() => closeCustomModal()} appearance="subtle">
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      );
    }
  }
   
  export default CustomModal
