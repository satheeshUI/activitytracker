import React from 'react';
import { Container, Header, Content } from 'rsuite';

// pages
import UserList from './modules/Users';
import SideNavBar from './components/SideNavBar';


  
  class Layout extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        expand: true
      };
      this.handleToggle = this.handleToggle.bind(this);
    }
    handleToggle() {
      this.setState({
        expand: !this.state.expand
      });
    }
    render() {
      const { expand } = this.state;
      return (
        <div className="show-fake-browser sidebar-page" style={{ border: '0' }}>
          <Container>
            <SideNavBar expand={expand} handleToggle={this.handleToggle} />

            <Container>
              <Header>
                <h5 className="page-title">Users List</h5>
              </Header>
              <Content>

                  {/* Page routers will come here */}
                  
                    {/* users list page */}
                    <UserList />

              </Content>
            </Container>
          </Container>
        </div>
      );
    }
  }
  
  export default Layout