import React from 'react';
import User from './../components/User';
import UserList from './../mock/listusers.json';

class UsersList extends React.Component {    
    render() {
        return(
            <div className="userslist-wrapper">
                <User UserList={UserList.members} />
            </div>
        )
    }
}

export default UsersList