import React from 'react';
import './assets/scss/main.scss';
import 'rsuite/dist/styles/rsuite-default.css';
import './assets/scss/theme-default.css';
import Layout from './Layout';

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
